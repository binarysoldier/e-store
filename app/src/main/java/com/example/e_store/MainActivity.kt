package com.example.e_store

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log.d
import android.view.Menu
import android.view.MenuItem
import androidx.core.view.GravityCompat
import androidx.room.Room
import com.example.e_store.database.AppDatabase
import com.example.e_store.database.ProductFromDatabase
import kotlinx.android.synthetic.main.activity_main.*
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

                doAsync{
                    val db = Room.databaseBuilder(
                        applicationContext,
                        AppDatabase::class.java, "database-name"
                    ).build()

                    db.productDao().insertAll(ProductFromDatabase(null, "Socks - one dozen", 1.99))
                    val products = db.productDao().getAll()

                    uiThread {

                        d("daniel", "products size? ${products.size} ${products[0].title}")

            }
        }

        supportFragmentManager.beginTransaction()
            .replace(R.id.frameLayout, MainFragment())
            .commit()

        navigationView.setNavigationItemSelectedListener {
            when (it.itemId){
                R.id.actionHome -> {
                    supportFragmentManager.beginTransaction()
                        .replace(R.id.frameLayout, MainFragment())
                        .commit()
                }
                R.id.actionKeyboards -> {
                    supportFragmentManager.beginTransaction()
                        .replace(R.id.frameLayout, KeyboardFragment())
                        .commit()
                }

                R.id.actionTelephones -> {
                    supportFragmentManager.beginTransaction()
                        .replace(R.id.frameLayout, TelephoneFragment())
                        .commit()
                }
                R.id.actionPC -> {
                    supportFragmentManager.beginTransaction()
                        .replace(R.id.frameLayout, PcFragment())
                        .commit()
                }
                R.id.actionGaming -> {
                    supportFragmentManager.beginTransaction()
                        .replace(R.id.frameLayout, GamingFragment())
                        .commit()
                }
                R.id.actionAdmin -> {
                    supportFragmentManager.beginTransaction()
                        .replace(R.id.frameLayout, AdminFragment())
                        .commit()
                }
                R.id.actionSettings -> d("daniel", "Settings was prepared" )
            }
            it.isChecked = true
            drawerLayout.closeDrawers()
            true
        }

        supportActionBar?.apply{
            setDisplayHomeAsUpEnabled(true)
            setHomeAsUpIndicator(R.drawable.ic_menu_black_24dp)
        }
 }
    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == R.id.actionCart){
            return true
        }
        drawerLayout.openDrawer(GravityCompat.START)
        return true
//      return super.onOptionsItemSelected(item)
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_toolbar, menu)
        return true
    }
}